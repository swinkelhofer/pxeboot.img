FROM alpine:3.18

RUN apk add perl xz-dev build-base git qemu-img syslinux dosfstools bash
COPY create_pxebootimg /usr/bin/create_pxebootimg
COPY config /config

WORKDIR /src
RUN git clone https://github.com/ipxe/ipxe && \
    cd ipxe/src && \
    cp /config/infinite.ipxe . && \
    # sed -i -e "s:#define LOG_LEVEL       LOG_NONE:#define LOG_LEVEL       LOG_ALL:g" -e "s://#undef        CONSOLE_PCBIOS:#define CONSOLE_PCBIOS CONSOLE_USAGE_ALL:g" config/console.h && \
    make bin/ipxe.lkrn EMBED=infinite.ipxe && \
    cp bin/ipxe.lkrn /bin/ipxe.krn

VOLUME /export
WORKDIR /export

ENTRYPOINT ["/usr/bin/create_pxebootimg"]
